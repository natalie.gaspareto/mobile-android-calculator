package com.example.calculadora;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NavUtils;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;


public class MainActivity extends AppCompatActivity {


    float numUm, numDois;

    float cont;

    boolean Soma, Subtrair, Multiplicar, Divisao;

    Button  btnSoma, btnMenos, btnDiv, btnMult, btnPonto, btnC, btnIgual,
            btn0, btn1, btn2, btn3, btn4, btn5, btn6, btn7, btn8, btn9;

    EditText cEditText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btn0 = (Button) findViewById(R.id.num0);
        btn1 = (Button) findViewById(R.id.num1);
        btn2 = (Button) findViewById(R.id.num2);
        btn3 = (Button) findViewById(R.id.num3);
        btn4 = (Button) findViewById(R.id.num4);
        btn5 = (Button) findViewById(R.id.num5);
        btn6 = (Button) findViewById(R.id.num6);
        btn7 = (Button) findViewById(R.id.num7);
        btn8 = (Button) findViewById(R.id.num8);
        btn9 = (Button) findViewById(R.id.num9);

        btnPonto = (Button) findViewById(R.id.ponto);
        btnSoma = (Button) findViewById(R.id.adicao);
        btnMenos = (Button) findViewById(R.id.subtracao);
        btnMult = (Button) findViewById(R.id.multiplicacao);
        btnDiv = (Button) findViewById(R.id.divisao);
        btnC = (Button) findViewById(R.id.reset);
        btnIgual = (Button) findViewById(R.id.igual);
        cEditText = (EditText) findViewById(R.id.result);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cEditText.setText(cEditText.getText() + "1");
            }
        });

        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cEditText.setText(cEditText.getText() + "2");
            }
        });

        btn3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cEditText.setText(cEditText.getText() + "3");
            }
        });

        btn4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cEditText.setText(cEditText.getText() + "4");
            }
        });

        btn5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cEditText.setText(cEditText.getText() + "5");
            }
        });

        btn6.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cEditText.setText(cEditText.getText() + "6");
            }
        });

        btn7.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cEditText.setText(cEditText.getText() + "7");
            }
        });

        btn8.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cEditText.setText(cEditText.getText() + "8");
            }
        });

        btn9.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cEditText.setText(cEditText.getText() + "9");
            }
        });

        btn0.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cEditText.setText(cEditText.getText() + "0");
            }
        });

        btnSoma.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {


                    if (cEditText == null) {
                        cEditText.setText("");
                    } else {
                        numUm = Float.parseFloat(cEditText.getText() + "");
                        Soma = true;
                        cEditText.setText(null);
                    }
                } catch (NumberFormatException e) {
                    cEditText.setText("Inválido");
                }
            }
        });

        btnMenos.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                try {


                    numUm = Float.parseFloat(cEditText.getText() + "");
                    Subtrair = true;
                    cEditText.setText(null);
                } catch (NumberFormatException e){
                    cEditText.setText("Inválido");
                }
            }
        });

        btnDiv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    numUm = Float.parseFloat(cEditText.getText() + "");
                    Divisao = true;
                    cEditText.setText(null);
                } catch (NumberFormatException e){
                    cEditText.setText("Inválido");
                }
            }

        });

        btnMult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {

                    numUm = Float.parseFloat(cEditText.getText() + "");
                    Multiplicar = true;
                    cEditText.setText(null);
                } catch (NumberFormatException e){
                    cEditText.setText("Inválido");
                }
            }
        });

        btnIgual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {


                    numDois = Float.parseFloat(cEditText.getText() + "");

                    if (Soma == true) {
                        cEditText.setText(numUm + numDois + "");
                        Soma = false;
                    }

                    if (Subtrair == true) {
                        cEditText.setText(numUm - numDois + "");
                        Subtrair = false;
                    }

                    if (Multiplicar == true) {
                        cEditText.setText(numUm * numDois + "");
                        Multiplicar = false;
                    }

                    if (Divisao == true) {
                        cEditText.setText(numUm / numDois + "");
                        Divisao = false;
                    }
                } catch (NumberFormatException e) {
                    cEditText.setText("");
                }
            }
        });

        btnC.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cEditText.setText("");
            }
        });

        btnPonto.setOnClickListener(new View.OnClickListener() {
            @Override

            public void onClick(View v) {
                try {
                    cEditText.setText(cEditText.getText() + ".");
                } catch (NumberFormatException e) {
                    cEditText.setText("");
                }
            }
        });
    }
}
